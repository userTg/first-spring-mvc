package org.eclipse.firstspringmvc.controller;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
//	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
//	@RequestMapping(value="/hello" , method = RequestMethod.GET)
//	public void sayHello() {  
//		System.out.println("hello world");
//	}
	
	
//	@GetMapping(value="/hello")
//	public void sayHello() {  
//		System.out.println("hello world");
//	}
	
	
//	@GetMapping(value = "/hello")
//	public void sayHello(@RequestParam(value = "nom" , required = false, defaultValue = "wick") String s) {  
//		System.out.println("Hello " + s);
//	public String sayHello(Model model) { 
//		model.addAttribute("prenom", "John");
//		model.addAttribute("nom", "Wick");
//		return "hello";
//		
//	}
	
	@GetMapping(value = "/hello")
	public ModelAndView sayHello(ModelAndView mv) {
	mv.setViewName("hello");
	mv.addObject("prenom", "John");
	mv.addObject("nom", "Wick");
	return mv;
	}
	
	/**
	 * Simply selects the home view to render by returning its name.
//	 */
//	@RequestMapping(value = "/", method = RequestMethod.GET)
//	public String home(Locale locale, Model model) {
//		logger.info("Welcome home! The client locale is {}.", locale);
//		
//		Date date = new Date();
//		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
//		
//		String formattedDate = dateFormat.format(date);
//		
//		model.addAttribute("serverTime", formattedDate );
//		
//		return "home";
//	}
//	
//	@RequestMapping(value = "/contact", method = RequestMethod.GET)
//	public String contact(Locale locale, Model model) {
//		logger.info("Welcome home! The client locale is {}.", locale);
//		
//		Date date = new Date();
//		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
//		
//		String formattedDate = dateFormat.format(date);
//		
//		model.addAttribute("serverTime", formattedDate );
//		
//		return "contact";
//	}
}
