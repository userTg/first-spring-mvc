<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/file.css" />
<title>Result page</title>
</head>
<body>
	<h1>List of persons</h1>

	<div>

		<table border="1" align="center">
			<tr>
				<th>numéro</th>
				<th>prénom</th>
				<th>nom</th>
			</tr>
			<tr>
				<c:forEach items="${ personnes }" var="personne">
					<td><c:out value="${ personne.num } " /></td>
					<td><c:out value="${ personne.prenom } " /></td>
					<td><c:out value=" ${ personne.nom }" /></td>
			</tr>
			</c:forEach>
			<tr>
				<td><c:out value="${personneA.num } " /></td>
				<td><c:out value="${personneA.prenom } " /></td>
				<td><c:out value="${personneA.nom } " /></td>
			</tr>
		</table>

	</div>

</body>
</html>