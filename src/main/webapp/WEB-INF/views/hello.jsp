<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/file.css" />
<title>first jsp called from controller</title>
</head>
<body>
	<h1>first jsp called from controller</h1>
	<p class=first>je m'appelle ${prenom} ${nom} </p>
</body>
</html>